# frozen_string_literal: true

name 'test-cookbook'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/test-cookbook@incoming.gitlab.com'
license 'Apache-2.0'
description 'Cookbook used to test some chef behaviors'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/test-cookbook'
issues_url 'https://gitlab.com/chef-platform/test-cookbook/issues'
version '1.4.0'

supports 'centos', '>= 7.1'
supports 'arch'
supports 'debian', '>= 8.0'

chef_version '>= 12.19'
