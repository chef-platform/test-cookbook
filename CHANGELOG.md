Changelog
=========

1.3.0
-----

- Check that new knife share command did no break release management

1.2.0
-----

- Check that previous commit did not break release management

1.1.0
-----

- Nothing, just to test the release automation

1.0.0
-----

- Initial version, released for test
